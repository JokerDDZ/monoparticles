﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using MonoParticles.MinMaxValue;
using MonoParticles.ParticleEngineN;
using System;

namespace Test
{
    [TestClass]
    public class ParticleEngineTest
    {
        [TestMethod]
        public void RandomDir_RandomVectorDirection_VectorLengthEqualOne()
        {
            ParticleEngine engine = new ParticleEngine();
            engine.SetRandom = new Random();
            engine.Velocity = new MonoParticles.MinMaxValue.Velocity() { Min = 1f, Max = 5f };

            for (int i = 0; i < 100; i++)
            {
                Vector2 randomVector = engine.RandomDir();
                double length = randomVector.Length();
                length = Math.Round(length);
                Assert.AreEqual(length, 1f);
            }
        }
    }
}
