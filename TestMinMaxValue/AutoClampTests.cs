using Microsoft.VisualStudio.TestTools.UnitTesting;
using MonoParticles.MinMaxValue;

namespace Test
{
    [TestClass]
    public class AutoClampTests
    {
        #region Update
        [TestMethod]
        public void Update_MinIsGreaterThanMax_ReturnsTrueAndMinEqualMax()
        {
            AutoClampVariable autoClamp = new AutoClampVariable();
            autoClamp.Min = 2;
            autoClamp.Max = 1;

            Assert.IsTrue(autoClamp.Update());
            Assert.AreEqual(autoClamp.Min, autoClamp.Max);
        }

        [TestMethod]
        public void Update_MaxIsGreaterThanMin_ReturnFalse()
        {
            AutoClampVariable autoClamp = new AutoClampVariable();
            autoClamp.Min = 1;
            autoClamp.Max = 2;

            Assert.IsFalse(autoClamp.Update());
        }

        [TestMethod]
        public void Update_MaxIsGreaterThanMaximum_ReturnTrueAndMaxEqualMaximum()
        {
            //Maximum is const and equals 1000
            AutoClampVariable autoClamp = new AutoClampVariable();
            autoClamp.Max = 100000;

            Assert.IsTrue(autoClamp.Update());
            Assert.AreEqual(autoClamp.Max,autoClamp.Maximum);
        }

        [TestMethod]
        public void Update_MinIsLessThanMinimum_ReturnTrueAndMinEqualMinimum()
        {
            //Minimum is const and equals 0
            AutoClampVariable autoClamp = new AutoClampVariable();
            autoClamp.Min = -100000;

            Assert.IsTrue(autoClamp.Update());
            Assert.AreEqual(autoClamp.Min, autoClamp.Minimum);
        }

        [TestMethod]
        public void Update_MinIsGreaterThanMinimum_ReturnFalse()
        {
            //Maximum is const and equals 0
            AutoClampVariable autoClamp = new AutoClampVariable();
            autoClamp.Min = 1;
            autoClamp.Max = 2;

            Assert.IsFalse(autoClamp.Update());
        }
        #endregion

        #region Copy
        [TestMethod]
        public void Copy_MinAndMaxAreDiffrentInTwoObjects_MinAndMaxFromTwoObjectsAreEqual()
        {
            AutoClampVariable autoClampA = new AutoClampVariable();
            autoClampA.Min = 1;
            autoClampA.Max = 2;

            AutoClampVariable autoClampB = new AutoClampVariable();
            autoClampB.Min = 3;
            autoClampB.Max = 4;

            autoClampB.Copy(autoClampA);

            Assert.AreEqual(autoClampA.Min, autoClampB.Min);
            Assert.AreEqual(autoClampA.Max, autoClampB.Max);
        }
        #endregion
    }
}
