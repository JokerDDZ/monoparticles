﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using MonoParticles.ParticleEngineN;

namespace Test
{
    [TestClass]
    public class ParticleTest
    {
        [TestMethod]
        public void BaseUpdate_ChangePositionBasedOnConstVelocity_CorrectPositionBasedOnNumberOfFrames()
        {
            Particle particle = new Particle
            {
                Position = Vector2.Zero,
                Velocity = new Vector2(1f, 0f)
            };

            for(int i = 0; i < 100;i++)
            {
                particle.BaseUpdate(1f);
            }

            Assert.AreEqual(particle.Position,new Vector2(100f,0));
        }

        [TestMethod]
        public void BaseUpdate_PositionFrameIndependent_CorrectPosition()
        {
            Particle particle = new Particle
            {
                Position = Vector2.Zero,
                Velocity = new Vector2(1f, 0f)
            };

            for (int i = 0; i < 100; i++)
            {
                particle.BaseUpdate(1f);
            }

            Assert.AreEqual(particle.Position, new Vector2(100f, 0));

            particle.Position = Vector2.Zero;

            for (int i = 0; i < 400; i++)
            {
                particle.BaseUpdate(.25f);
            }

            Assert.AreEqual(particle.Position, new Vector2(100f, 0));
        }

        [TestMethod]
        public void BaseUpdate_VelocityChangeBasedOnGravity_CorrectVelocity()
        {
            Particle particle = new Particle
            {
                Position = Vector2.Zero,
                Velocity = new Vector2(0f, 10f),
                Gravity = -1f
            };

            for (int i = 0; i < 10; i++)
            {
                particle.BaseUpdate(1f);
            }

            Assert.AreEqual(particle.Velocity, Vector2.Zero);
        }

        [TestMethod]
        public void BaseUpdate_AngleChangeBasedOnAngularVelocity_CorrectAngle()
        {
            Particle particle = new Particle
            {
                Angle = 0f,
                AngularVelocity = 1f
            };

            for (int i = 0; i < 10; i++)
            {
                particle.BaseUpdate(1f);
            }

            Assert.AreEqual(particle.Angle, 10f);
        }

        [TestMethod]
        public void BaseUpdate_PositionBasedOnGravityAndVelocity_CorrectPosition()
        {
            Particle particle = new Particle
            {
                Position = Vector2.Zero,
                Velocity = new Vector2(0f, 10f),
                Gravity = -1f
            };

            for (int i = 0; i < 10; i++)
            {
                particle.BaseUpdate(1f);
            }

            Assert.AreEqual(particle.Position, new Vector2(0f,50f));
        }

        [TestMethod]
        public void BaseUpdate_LifeTimeCount_LifeTimeEqualZero()
        {
            Particle particle = new Particle
            {
                TTL = 10f
            };

            for (int i = 0; i < 10; i++)
            {
                particle.BaseUpdate(1f);
            }

            Assert.AreEqual(particle.TTL, 0f);
        }
    }
}
