﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using MonoParticles.Counters;

namespace Test
{
    [TestClass]
    public class FrameCountertest
    {
        [TestMethod]
        public void Update_OneSecondBetweenFrames_CurrenFramesEqualsOne()
        {
            FrameCounter frameCounter = new FrameCounter();

            frameCounter.Update(1f);
            Assert.AreEqual(frameCounter.CurrentFramesPerSecond,1f);
        }

        [TestMethod]
        public void Update_HalfSecondBetweenFrames_CurrenFramesEqualsTwo()
        {
            FrameCounter frameCounter = new FrameCounter();

            frameCounter.Update(0.5f);
            Assert.AreEqual(frameCounter.CurrentFramesPerSecond, 2f);
        }

        [TestMethod]
        public void Update_AvarageFramesPerSecond_AvarageNumber()
        {
            //For 100 Samples
            FrameCounter frameCounter = new FrameCounter();

            for(int i = 0; i < 101;i++)
            {
                if (i < 51)
                {
                    frameCounter.Update(1f);
                }
                else
                {
                    frameCounter.Update(.5f);
                }
            }
   
            Assert.AreEqual(frameCounter.AverageFramesPerSecond,1.5f);
        }
    }
}
