﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using MonoParticles.MinMaxValue;
using System.Collections.Generic;
using System.Diagnostics;

namespace Test
{
    [TestClass]
    public class ColorChangeTest
    {
        [TestMethod]
        public void SetColor_ColorNormalized_SetColorRedBasedOnNormalizeValue()
        {
            ColorChange colorChange = new ColorChange();
            //Red
            List<float> startRedColor = new List<float> { 255f, 0f, 0f };
            //Green
            List<float> endGreenColor = new List<float>{0f, 255f, 0f};
           
            Assert.IsTrue(colorChange.SetColor(startRedColor, endGreenColor));
            Assert.AreEqual(colorChange.StartColor,Color.Red);
            Assert.AreEqual(colorChange.EndColor,new Color(0f,1f,0f));
        }

        [TestMethod]
        public void SetColor_WrongInputList_ReturnFalseSetDefaultColorWhite()
        {
            ColorChange colorChange = new ColorChange();
            //Red
            List<float> startRedColor = new List<float> { 255f, 0f, 0f ,4f,5f};
            //Green
            List<float> endGreenColor = new List<float> { 0f, 255f, 0f };

            Assert.IsFalse(colorChange.SetColor(startRedColor, endGreenColor));
            Assert.AreEqual(colorChange.StartColor, Color.White);
            Assert.AreEqual(colorChange.EndColor, Color.White);
        }
    }
}
