﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Timers;
using MonoParticles.MinMaxValue;

namespace MonoParticles.ParticleEngineN
{
    public class ParticleEngine
    {      
        public Vector2 EmitterLocation { get; set; }
        public Velocity Velocity { get; set; }
        public AngularVelocity AngularVelocity { get; set; }
        public Size Size { get; set; }
        public LifeTime LifeTime { get; set; }
        public double Gravity { get; set; }
        public double Density { get; set; } = 1f;
        public int Limit { get; set; }
        public bool RandomColor { get; set; }
        public ColorChange ColorChange { get; set; }
        public int NumberOfParticles { get; private set; } = 0;

        private List<Particle> _particles;
        private List<Texture2D> _textures;
        private double _timeToNextParticle = 0;

        private Random _random { get; set; }
        public Random SetRandom { set { _random = value; } }

        public ParticleEngine() { }

        public ParticleEngine(List<Texture2D> textures, Vector2 location)
        {
            EmitterLocation = location;
            this._textures = textures;
            this._particles = new List<Particle>();
            _random = new Random();
        }

        private Particle GenerateNewParticle(float lifeTime)
        {
            Texture2D texture = _textures[0];
            Vector2 position = EmitterLocation;

            Vector2 velocity = RandomizeVelocity();

            float angle = 0;
            float angularVelocity = (float)GetRandomNumber(AngularVelocity.Min,AngularVelocity.Max);
            Color color = new Color(
                    (float)_random.NextDouble(),
                    (float)_random.NextDouble(),
                    (float)_random.NextDouble());
            float size = (float)GetRandomNumber(Size.Min, Size.Max);
            float ttl = lifeTime;

            return new Particle(texture, position, velocity, angle, angularVelocity, color, size, ttl,RandomColor);
        }

        public void Update(GameTime gameTime)
        {
            double deltaTime = gameTime.ElapsedGameTime.TotalSeconds;
            double requiredTimeToParticle = 1f / Density;
            _timeToNextParticle += deltaTime;

            if (_timeToNextParticle > requiredTimeToParticle)
            {
                int iteratorDebug = 0;

                while (_timeToNextParticle > requiredTimeToParticle)
                {
                    _particles.Add(GenerateNewParticle((float)GetRandomNumber(LifeTime.Min, LifeTime.Max)));
                    _timeToNextParticle -= requiredTimeToParticle;
                    iteratorDebug++;
                }
            }

            for (int particle = 0; particle < _particles.Count; particle++)
            {
                _particles[particle].Gravity = Gravity;
                _particles[particle].Update(deltaTime,ColorChange);
                if (_particles[particle].TTL <= 0 || particle > Limit)
                {
                    _particles.RemoveAt(particle);
                    particle--;
                }
            }

            NumberOfParticles = _particles.Count;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            for (int index = 0; index < _particles.Count; index++)
            {
                _particles[index].Draw(spriteBatch);
            }
            spriteBatch.End();
        }

        public Vector2 RandomDir()
        {
            float x = 1f * (float)(_random.NextDouble() * 2 - 1);
            float y = 1f * (float)(_random.NextDouble() * 2 - 1);

            Vector2 dir = new Vector2(x, y);
            dir = Vector2.Normalize(dir);

            return dir;
        }

        public void ResetParticles()
        {
            _particles.Clear();
        }

        private Vector2 RandomizeVelocity()
        {
            Vector2 velocity = RandomDir();
            velocity *= (float)GetRandomNumber(Velocity.Min, Velocity.Max);
            return velocity;
        }

        private double GetRandomNumber(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }
    }
}
