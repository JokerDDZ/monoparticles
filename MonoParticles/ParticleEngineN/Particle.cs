﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using System;
using MonoParticles.MinMaxValue;

namespace MonoParticles.ParticleEngineN
{
    public class Particle
    {
        public Texture2D Texture { get; set; }        
        public Vector2 Position { get; set; }            
        public Vector2 Velocity { get; set; }        
        public float Angle { get; set; }            
        public float AngularVelocity { get; set; }    
        public Color Color { get; set; }            
        public float Size { get; set; }                
        public double TTL { get; set; }
        private double _startLifeTime { get; set; }
        public double Gravity { get; set; } = 0f;
        private bool _randomColor { get; set; }

        public Particle() { }

        public Particle(Texture2D texture, Vector2 position, Vector2 velocity,
            float angle, float angularVelocity, Color color, float size, float ttl,bool randomColor)
        {
            Texture = texture;
            Position = position;
            Velocity = velocity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            Color = color;
            Size = size;
            TTL = ttl;
            _startLifeTime = TTL;
            _randomColor = randomColor;
        }

        public void Update(double deltaTime,ColorChange color)
        {
            BaseUpdate(deltaTime);

            if(!_randomColor)
            {
                double normalizedLifeTime = TTL / _startLifeTime;
                Color start = color.StartColor;
                Color end = color.EndColor;
                Color = Color.Lerp(end,start,(float)normalizedLifeTime);
            }
        }

        public void BaseUpdate(double deltaTime)
        {
            Vector2 gravity = new Vector2(0f, (float)Gravity);
            float deltaTimeToFloat = (float)deltaTime;
            TTL -= deltaTime;
            Position += (Velocity + 0.5f * gravity * deltaTimeToFloat) * deltaTimeToFloat;
            Velocity += gravity * (float)deltaTime;
            Angle += AngularVelocity * (float)deltaTime;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Rectangle sourceRectangle = new Rectangle(0, 0, Texture.Width, Texture.Height);
            Vector2 origin = new Vector2(Texture.Width / 2, Texture.Height / 2);

            spriteBatch.Draw(Texture, Position, sourceRectangle, Color,
                Angle, origin, Size, SpriteEffects.None, 0f);
        }
    }
}
