﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonoParticles.MinMaxValue
{
    public class Size: AutoClampVariable
    {
        public override double Min { get; set; } = 5f;
        public override double Max { get; set; } = 10f;
    }
}
