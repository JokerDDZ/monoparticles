﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace MonoParticles.MinMaxValue
{
    public class ColorChange
    {
        public Color StartColor = Color.White;
        public Color EndColor = Color.White;

        public bool SetColor(List<float> startColor, List<float> endColor)
        {          
            if(startColor.Count != 3 || endColor.Count != 3)
            {
                StartColor = Color.White;
                EndColor = Color.White;

                return false;
            }

            StartColor = new Color(startColor[0]/255f,startColor[1]/255f,startColor[2]/255f,1f);
            EndColor = new Color(endColor[0]/255f,endColor[1]/255f,endColor[2]/255f,1f);
            return true;
        }
    }

}
