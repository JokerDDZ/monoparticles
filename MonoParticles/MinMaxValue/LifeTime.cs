﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonoParticles.MinMaxValue
{
    public class LifeTime : AutoClampVariable
    {
        public override double Min { get; set; } = 1f;
        public override double Max { get; set; } = 2f;
    }
}
