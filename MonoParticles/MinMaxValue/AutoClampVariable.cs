﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MonoParticles.MinMaxValue
{
    public class AutoClampVariable : IMinMaxValue
    {
        public virtual double Min { get; set; }
        public virtual double Max { get; set; }

        [Browsable(false)]
        public virtual double Minimum { get; } = 0f;
        [Browsable(false)]
        public virtual double Maximum { get; } = 1000f;

        public AutoClampVariable() { }
        
        public virtual bool Update()
        {
            bool result = false;

            if(Min > Max)
            {
                Min = Max;
                result = true;
            }

            if(Min < Minimum)
            {
                Min = Minimum;
                result = true;
            }

            if(Max < Minimum)
            {
                Max = Minimum;
                result = true;
            }

            if(Min > Maximum)
            {
                Min = Maximum;
                result = true;
            }

            if(Max > Maximum)
            {
                Max = Maximum;
                result = true;
            }

            return result;
        }

        public void Copy(AutoClampVariable clamp)
        {
            Min = clamp.Min;
            Max = clamp.Max;
        }
    }
}
