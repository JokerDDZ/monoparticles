﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonoParticles.MinMaxValue
{
    interface IMinMaxValue
    {
        double Min { get; set; }
        double Max { get; set; }
        double Minimum { get; }
        double Maximum { get; }
    }
}
