﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonoParticles.MinMaxValue
{
    public class Velocity : AutoClampVariable
    {
        public override double Min { get; set; } = 5f;
        public override double Max { get; set; } = 60f;
        public override double Maximum { get;} = 100000f;
    }
}
