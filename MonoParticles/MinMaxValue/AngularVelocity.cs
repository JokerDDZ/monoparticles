﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonoParticles.MinMaxValue
{
    public class AngularVelocity : AutoClampVariable
    {
        public override double Min { get; set; } = 2;
        public override double Max { get; set; } = 5;
        public override double Minimum { get; } = -2000f;
        public override double Maximum { get; } = 2000f;
    }
}
