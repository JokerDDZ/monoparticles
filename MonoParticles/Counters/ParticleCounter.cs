﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonoParticles.Counters
{
    class ParticleCounter : Counter
    {
        public ParticleCounter(Vector2 pos,Game1 game1) : base(pos,game1) { }
        private int _numberOfParticles { get; set; } = 0;

        public override void Update(float DeltaTime)
        {
        }

        public void Update(int numberOfParticles)
        {
            _numberOfParticles = numberOfParticles;
        }

        public override bool Draw(SpriteBatch spriteBatch)
        {           
            _textToDisplay = "Number of particles: " + _numberOfParticles;
            DrawText(spriteBatch);
            return true;
        }
    }
}
