﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonoParticles.Counters
{
    public abstract class Counter
    {
        protected SpriteFont _spriteFont { get; set; }
        protected SpriteBatch _spriteBatch { get; set; }
        protected Color _fontColor { get; set; }
        protected Vector2 _position { get; set; }
        protected string _textToDisplay { get; set; }

        public Counter(Vector2 position,Game1 game1)
        {
            _position = position;
            _spriteFont = game1.BasicFont;
            _fontColor = Color.White;
        }

        public Counter() { }

        public abstract bool Draw(SpriteBatch spriteBatch);

        public abstract void Update(float DeltaTime);

        public void SetFont(SpriteFont spriteFont, Color fontColor)
        {
            _spriteFont = spriteFont;
            _fontColor = fontColor;
        }

        protected virtual void DrawText(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(_spriteFont, _textToDisplay, _position, _fontColor);
            spriteBatch.End();
        }
    }
}
