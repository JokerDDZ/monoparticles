﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Myra;
using Myra.Graphics2D.UI;
using Myra.Graphics2D.UI.Properties;
using System.Collections.Generic;
using System.Diagnostics;
using MonoParticles.Counters;
using MonoParticles.ParticleEngineN;
using MonoParticles.GUI;

namespace MonoParticles
{
    public class Game1 : Game
    {
        public static Game1 Instance { get; set; }
        public SpriteBatch SpriteBatch { get; private set; }
        public ParticleEngine ParticleEngine { get; private set; }
        private GraphicsDeviceManager _graphics { get; set; }
        private FrameCounter _frameCounter { get; set; }
        private ParticleCounter _particleCounter { get; set; }
        private ParticleEditor _particleEditor { get; set; }
        public Panel Root { get; private set; } = new Panel();

        #region Load content
        public SpriteFont BasicFont { get; private set; }
        private Texture2D _basicParticle { get; set; }
        #endregion


        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
        }

        protected override void Initialize()
        {
            if(Instance != null)
            {
                Debug.WriteLine("Game1 object already exists");             
            }
            else
            {
                Instance = this;
            }
            
            _graphics.PreferredBackBufferWidth = 1366;
            _graphics.PreferredBackBufferHeight = 768;
            //_graphics.IsFullScreen = false;
            _graphics.ApplyChanges();

            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            BasicFont = Content.Load<SpriteFont>("DefaultFont");
            _basicParticle = Content.Load<Texture2D>("OnePixel");

            MyraEnvironment.Game = this;

            List<Texture2D> textrues = new List<Texture2D>();
            textrues.Add(_basicParticle);
            ParticleEngine = new ParticleEngine(textrues, new Vector2(400, 240));
            _particleEditor = new ParticleEditor(ParticleEngine, GraphicsDevice);
            _frameCounter = new FrameCounter(Vector2.One,this);
            _particleCounter = new ParticleCounter(new Vector2(1, 20),this);
   
            base.LoadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            _frameCounter.Update(deltaTime);
            _particleCounter.Update(ParticleEngine.NumberOfParticles);
            ParticleEngine.Update(gameTime);
            _particleEditor.Update(gameTime);
            
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            _frameCounter.Draw(SpriteBatch);
            _particleCounter.Draw(SpriteBatch);
            ParticleEngine.Draw(SpriteBatch);
            _particleEditor.Draw();

            base.Draw(gameTime);
        }
    }
}
