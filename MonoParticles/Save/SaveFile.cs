﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using static MonoParticles.ParticleEngineN.ParticleEngine;
using MonoParticles.ParticleEngineN;
using MonoParticles.GUI;
using Microsoft.Xna.Framework;

namespace MonoParticles.Save
{
    public class SaveFile
    {
        public ParticleEditor ParticleEditor;
        public static string Path { get; private set; }

        public SaveFile(ParticleEditor particleEditor)
        {
            if(string.IsNullOrEmpty(Path))
            {
                Path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/MonoParticlesSave";
            }

            ParticleEditor = particleEditor;
        }

        public static void SaveToJsonFile(ParticleEngine particleEngine,string name)
        {
            CheckDirectory();

            AdditionalDataSaver additionalDataSaver = new AdditionalDataSaver(particleEngine);

            string jsonStringMain = JsonSerializer.Serialize(particleEngine) + "AdditionalData";
            string jsonStringAdd = JsonSerializer.Serialize(additionalDataSaver);
            string jsonString = jsonStringMain + jsonStringAdd;

            string savePath = Path + "/" + name + ".JSON";

            Debug.WriteLine("Path: " + savePath);
            File.WriteAllText(savePath, jsonString);
        }

        public static FileInfo[] LoadAllFiles()
        {
            CheckDirectory();

            DirectoryInfo di = new DirectoryInfo(Path);
            FileInfo[] files = di.GetFiles("*.JSON");
            return files;
        }

        public void LoadEditor(string name)
        {
            string allInfo = File.ReadAllText(Path + "/" + name + ".JSON");
            Debug.WriteLine("All info: " + allInfo);
            string[] split = allInfo.Split("AdditionalData");
            //Debug.WriteLine();
            ParticleEditor particleEditorHandler = JsonSerializer.Deserialize<ParticleEditor>(split[0]);
            Debug.WriteLine("split0: " + split[0]);
            Debug.WriteLine("split1: " + split[1]);
            AdditionalDataSaver addData = JsonSerializer.Deserialize<AdditionalDataSaver>(split[1]);
            ParticleEditor.SetParticleEditor(particleEditorHandler,addData);
        }

        private static void CheckDirectory()
        {
            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }
        }


        public class AdditionalDataSaver
        {
            public List<float> ColorStart { get; set; } = new List<float>();
            public List<float> ColorEnd { get; set; } = new List<float>();

            public AdditionalDataSaver() { }

            public AdditionalDataSaver(ParticleEngine particleEngine)
            {
                Color start = particleEngine.ColorChange.StartColor;
                Color end = particleEngine.ColorChange.EndColor;

                ColorStart.Add(start.R);
                ColorStart.Add(start.G);
                ColorStart.Add(start.B);

                ColorEnd.Add(end.R);
                ColorEnd.Add(end.G);
                ColorEnd.Add(end.B);
            }
        }
    }
}
