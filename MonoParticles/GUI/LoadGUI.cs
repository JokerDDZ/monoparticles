﻿using Microsoft.Xna.Framework;
using Myra.Graphics2D.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using MonoParticles.Save;

namespace MonoParticles.GUI
{
    public class LoadGUI
    {
        public Window Window { get;private set; }
        private VerticalStackPanel _verticalStackPanel { get; set; }
        private ScrollViewer _scrollViewer { get; set; }
        private SaveFile _saveFile { get; set; }

        public LoadGUI(Desktop desktop,SaveFile saveFile)
        {
            _saveFile = saveFile;

            Label label = new Label
            {
                Text = "Load"
            };

            Grid grid = new Grid
            {
                DefaultColumnProportion = new Proportion
                {
                    Type = Myra.Graphics2D.UI.ProportionType.Auto,
                },
                DefaultRowProportion = new Proportion
                {
                    Type = Myra.Graphics2D.UI.ProportionType.Auto,
                }
            };

            _verticalStackPanel = new VerticalStackPanel();

            _scrollViewer = new ScrollViewer();
            _scrollViewer.Content = _verticalStackPanel;

            Window = new Window
            {
                Title = "Load",
                Content = _scrollViewer,
                Width = 100,
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Center,
            };

            Point point = new Point(Window.Bounds.X, Window.Bounds.Y);
            Window.Show(desktop, point);
        }

        public void AddButtonsBasedOnLoadFiles(FileInfo[] files)
        {
            _verticalStackPanel.Widgets.Clear();

            foreach (FileInfo file in files)
            {
                string name = file.Name.Replace(".JSON","");
                TextButton button = new TextButton() { Text = name};
                button.Click += LoadEditorPressed;
                _verticalStackPanel.Widgets.Add(button);
            }
        }

        private void LoadEditorPressed(object sender, System.EventArgs e)
        {
            TextButton button = (TextButton)sender;
            _saveFile.LoadEditor(button.Text);
        }
    }
}
