﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Myra.Graphics2D.UI.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.ComponentModel;
using Myra.Utility;
using Myra.Graphics2D.UI;
using MonoParticles.MinMaxValue;
using MonoParticles.ParticleEngineN;
using MonoParticles.Save;

namespace MonoParticles.GUI
{
    public class ParticleEditor
    {
        [Browsable(false)]
        public PropertyGrid PropertyGrid { get; set; }

        public double Density { get; set; } = 60f;
        [Category("Velocity")]
        public double Gravity { get; set; } = 0f;
        [Category("Velocity")]
        public Velocity Velocity { get; set; }
        [Category("Velocity")]
        public AngularVelocity AngularVelocity { get; set; }
        public Size Size { get; set; }
        public LifeTime LifeTime { get; set; }
        public int Limit { get; set; } = 40000;

        private LoadGUI _loadGUI { get; set; }

        #region Color
        [Category("Color")]
        public ColorChange ColorChange { get; set; } = new ColorChange();
        [Category("Color")]
        public bool RandomColor { get; set; } = true;
        #endregion

        [Category("SaveFile")]
        public string SaveFileName;
        
        #region Buttons
        private TextButton _showButton { get; set; }
        private TextButton _resetButton { get; set; }
        private TextButton _followButton { get; set; }
        private TextButton _saveButton { get; set; }
        #endregion

        private SaveFile _saveFile { get; set; }
        private ParticleEngine _particleEngine { get; set; }
        private GraphicsDevice _graphicsDevice { get; set; }
        private List<AutoClampVariable> _autoClampVariables = new List<AutoClampVariable>();
        private Panel _root { get; set; } = new Panel();
        private Desktop _desktop { get; set; }
        private Window _window { get; set; }
        private bool _followMouse { get; set; }

        private bool _clampValueChanged = false;

        public ParticleEditor() { }

        public ParticleEditor(ParticleEngine particleEngine, GraphicsDevice graphicsDevice)
        {
            _particleEngine = particleEngine;
            _graphicsDevice = graphicsDevice;

            PropertyGrid = new PropertyGrid
            {
                Object = this,
                Width = 350,
            };

            Velocity = new Velocity();
            AngularVelocity = new AngularVelocity();
            Size = new Size();
            LifeTime = new LifeTime();

            _showButton = new TextButton
            {
                Text = "Show",
                Toggleable = true,
                VerticalAlignment = VerticalAlignment.Bottom,
                HorizontalAlignment = HorizontalAlignment.Right,
                Width = 50,
                Left = -5,
                Top = -10
            };

            _resetButton = new TextButton
            {
                Text = "Reset",
                Toggleable = false,
                VerticalAlignment = VerticalAlignment.Bottom,
                HorizontalAlignment = HorizontalAlignment.Right,
                Width = 55,
                Left = -65,
                Top = -10
            };

            _followButton = new TextButton
            {
                Text = "Follow Mouse",
                Toggleable = true,
                VerticalAlignment = VerticalAlignment.Bottom,
                HorizontalAlignment = HorizontalAlignment.Right,
                Width = 100,
                Left = -130,
                Top = -10
            };

            _saveButton = new TextButton
            {
                Text = "Save",
                Toggleable = false,
                VerticalAlignment = VerticalAlignment.Bottom,
                HorizontalAlignment = HorizontalAlignment.Right,
                Width = 50,
                Left = -245,
                Top = -10
            };

            _root.Widgets.Add(_showButton);
            _root.Widgets.Add(_resetButton);
            _root.Widgets.Add(_followButton);
            _root.Widgets.Add(_saveButton);

            _window = new Window
            {
                Title = "Particle Editor",
                Content = PropertyGrid,
                HorizontalAlignment = HorizontalAlignment.Right,
                VerticalAlignment = VerticalAlignment.Top,
            };

            _desktop = new Desktop();
            _desktop.Root = _root;
            Point point = new Point(_window.Bounds.X, _window.Bounds.Y);
            _window.Show(_desktop, point);

            _window.Closed += (s, a) =>
            {
                _showButton.IsPressed = false;
            };

            // Force window show
            _showButton.IsPressed = true;
            _followButton.IsPressed = false;

            ConnectEvents();

            _particleEngine.LifeTime = LifeTime;
            _particleEngine.Velocity = Velocity;
            _particleEngine.AngularVelocity = AngularVelocity;
            _particleEngine.Size = Size;

            _autoClampVariables.Add(LifeTime);
            _autoClampVariables.Add(Velocity);
            _autoClampVariables.Add(AngularVelocity);
            _autoClampVariables.Add(Size);

            _saveFile = new SaveFile(this);

            _loadGUI = new LoadGUI(_desktop,_saveFile);
            _loadGUI.AddButtonsBasedOnLoadFiles(SaveFile.LoadAllFiles());

            PropertyGrid.Rebuild();
        }

        public void Update(GameTime gameTime)
        {
            if (_followMouse)
            {
                _particleEngine.EmitterLocation = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
            }
            else
            {
                Vector2 screenCenter = new Vector2(_graphicsDevice.Viewport.Bounds.Width / 2, _graphicsDevice.Viewport.Bounds.Height / 2);
                _particleEngine.EmitterLocation = screenCenter;
            }

            foreach (AutoClampVariable variable in _autoClampVariables)
            {
                if(variable.Update())
                {
                    _clampValueChanged = true;
                }
            }

            Density = ClampValue(Density,0f,20000);
            Gravity = ClampValue(Gravity, -10000f, 10000f);
            Limit = ClampValue(Limit, 0, 60000);

            _particleEngine.Density = Density;
            _particleEngine.Limit = Limit;
            _particleEngine.Gravity = Gravity;
            _particleEngine.ColorChange = ColorChange;
            _particleEngine.RandomColor = RandomColor;

            if (_clampValueChanged)
            {
                _clampValueChanged = false;
                PropertyGrid.Rebuild();
            }
        }

        public void Draw()
        {
            _desktop.Render();
        }

        public void ConnectEvents()
        {
            _resetButton.PressedChanged += ResetParticles;
            _showButton.PressedChanged += ShowButtonPressedChanged;
            _followButton.PressedChanged += FollowButtonPressedChanged;
            _saveButton.Click += SaveButtonPressed;
        }

        public void SetParticleEditor(ParticleEditor particleEditor,SaveFile.AdditionalDataSaver addData)
        {
            Density = particleEditor.Density;
            Gravity = particleEditor.Gravity;
            Velocity.Copy(particleEditor.Velocity);
            AngularVelocity.Copy(particleEditor.AngularVelocity);
            Size.Copy(particleEditor.Size);
            LifeTime.Copy(particleEditor.LifeTime);
            Limit = particleEditor.Limit;
            ColorChange.SetColor(addData.ColorStart,addData.ColorEnd);
            RandomColor = particleEditor.RandomColor;

            _particleEngine.ResetParticles();
            PropertyGrid.Rebuild();
        }

        private void ResetParticles(object sender, EventArgs e)
        {
            _particleEngine.ResetParticles();
            PropertyGrid.Rebuild();
        }

        #region ClampValue
        private double ClampValue(double value, double min, double max)
        {
            if (value < min)
            {
                value = min;
                _clampValueChanged = true;
            }
            else if (value > max)
            {
                value = max;
                _clampValueChanged = true;
            }

            return value;
        }

        private int ClampValue(int value, int min, int max)
        {
            if (value < min)
            {
                value = min;
                _clampValueChanged = true;
            }
            else if (value > max)
            {
                value = max;
                _clampValueChanged = true;
            }

            return value;
        }
        #endregion
        #region Buttons
        private void ShowButtonPressedChanged(object sender, System.EventArgs e)
        {
            TextButton button = (TextButton)sender;

            if (button.IsPressed)
            {
                _loadGUI.Window.Show(_desktop);
                _window.Show(_desktop);
            }
            else
            {
                _loadGUI.Window.Close();
                _window.Close();
            }
        }

        private void FollowButtonPressedChanged(object sender, System.EventArgs e)
        {
            TextButton button = (TextButton)sender;

            if (button.IsPressed)
            {
                _followMouse = true;
            }
            else
            {
                _followMouse = false;
            }
        }

        private void SaveButtonPressed(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(SaveFileName))
            {
                SaveFile.SaveToJsonFile(_particleEngine, SaveFileName);
            }

            _loadGUI.AddButtonsBasedOnLoadFiles(SaveFile.LoadAllFiles());
        }
        #endregion
    }
}
